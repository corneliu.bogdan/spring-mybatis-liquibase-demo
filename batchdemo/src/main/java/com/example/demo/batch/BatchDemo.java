package com.example.demo.batch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BatchDemo {

    public static void main(String args[]) {
        SpringApplication.run(BatchDemo.class, args);
    }

}
