package com.example.demo.camel;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.file.GenericFile;
import org.apache.camel.impl.DefaultCamelContext;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.StringBufferInputStream;

public class CamelDemo {

    public static void main(String[] args) throws Exception {

        CamelContext camelContext = new DefaultCamelContext();

        camelContext.addRoutes(new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                from("file:/temp/camelDemoInput").
                        process(new MyProcessor()).
                        to("file:/temp/camelDemoOutput");
            }
        });

        camelContext.start();

        Thread.sleep(10 * 60 * 1000);

        camelContext.stop();
    }

    public static class MyProcessor implements Processor {

        @Override
        public void process(Exchange exchange) throws Exception {
            String filePath = ((GenericFile)exchange.getIn().getBody()).getAbsoluteFilePath();
            BufferedReader fileReader = new BufferedReader(new FileReader(filePath));
            String line;
            StringBuffer stringBuffer = new StringBuffer();
            while ((line = fileReader.readLine()) != null) {
                stringBuffer.append("___" + line + "__").append('\n');
            }
            fileReader.close();
            StringBufferInputStream bufferedInputStream = new StringBufferInputStream(stringBuffer.toString());
            exchange.getOut().setBody(bufferedInputStream);

        }

    }
}
