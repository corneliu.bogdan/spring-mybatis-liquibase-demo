package com.example.demo.model.mappers;

import com.example.demo.model.Test;

import java.util.List;

public interface TestMapper {

    List<Test> findAll();
    void insert(Test test);
}
