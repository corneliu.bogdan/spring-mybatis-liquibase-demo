package com.example.demo.resources;


import com.example.demo.model.Test;
import com.example.demo.model.mappers.TestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/test")
public class TestResource {

    @Autowired
    private TestMapper testMapper;

    @RequestMapping(method = RequestMethod.GET)
    public List<Test> findAll() {
        return testMapper.findAll();
    }

}
